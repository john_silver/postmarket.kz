export class CategorySiteController {
  constructor () {
    'ngInject';

    var self = this;

    self.images2 = [
      {
        path: "maket1",
        caption: "Электроника 1"
      },
      {
        path: "maket2",
        caption: "Электроника 2"
      },
      {
        path: "maket3",
        caption: "Электроника 3"
      },
      {
        path: "maket4",
        caption: "Электроника 4"
      },
      {
        path: "maket5",
        caption: "Электроника 5"
      },
      {
        path: "maket6",
        caption: "Электроника 6"
      }
    ];

    self.images = [];

    self.items = [
      {
        display: 'Электроника'
      },{
        display: 'Электроника'
      },{
        display: 'Электроника'
      },{
        display: 'Электроника'
      }
    ];

    self.selectedItemChange = ()=>{
      self.images = self.images2;
    };
    self.click = (item)=>{
      console.log(item.show);
      item.show = !item.show ;
      console.log(item.show);
    }
  }


}
