export class ChoiceImageController {
  constructor ($state, $scope) {
    'ngInject';

    var self = this;

    self.images = [
      {
        path: "Product1",
        caption: "Электроника 1"
      },
      {
        path: "Product2",
        caption: "Электроника 2"
      },
      {
        path: "Product3",
        caption: "Электроника 3"
      },
      {
        path: "Product4",
        caption: "Электроника 4"
      }
    ];

    self.titleButton = "Загрузить картинки";
    self.title = "Загрузите картинки товаров";
    self.download = false;
    self.clickButton = ()=>{
      console.log('click');
      if (!self.download){
        //$StateProvider.state('')
        self.download = true;
      }else{
        $state.go('site')
      }
      self.title = "Выбранные картинки";
      self.titleButton = "Создать сайт";
      $scope.$apply();
    };
    self.clickButton2 = ()=>{
      if (!self.download){
      }else{
        $state.go('site')
      }
    }

    $scope.clickButton = self.clickButton;

  }


}
