export function routerConfig ($stateProvider, $urlRouterProvider) {
  'ngInject';
  $stateProvider
    .state('home', {
      url: '/',
      templateUrl: 'app/main/main.html',
      controller: 'MainController',
      controllerAs: 'main'
    })
    .state('typeSite',{
      url: '/typeSite/',
      templateUrl: 'app/typeSite/typeSite.html',
      controller: 'TypeSiteController',
      controllerAs: 'm'
    })
    .state('categorySite',{
      url: '/categorySite/',
      templateUrl: 'app/categorySite/categorySite.html',
      controller: 'CategorySiteController',
      controllerAs: 'm'
    })
    .state('choiceImage',{
      url: '/choiceImage/',
      templateUrl: 'app/choiсeImage/choiсeImage.html',
      controller: 'ChoiceImageController',
      controllerAs: 'm'
    })
    .state('site',{
      url: '/site/',
      templateUrl: 'app/site/site.html',
      controller: 'SiteController',
      controllerAs: 'm'
    });

  $urlRouterProvider.otherwise('/');
}
